*** Settings ***
Documentation   Customers testing using the service layer
Resource        ../../../resources/services.robot
Resource        ../../../resources/base.robot

*** Test Cases ***
Put Customer
    ${customer}         Get Json                customers/isadora.json

    Delete Costumer     ${customer['cpf']} 

    ${resp}             Post Costumer           ${customer} 

    ${customer_id}      Convert To String       ${resp.json()['id']} 

    Set To Dictionary   ${customer}             address     Rua Miguel Ferreira, 100

    ${resp}             Put Costumer            ${customer}         ${customer_id}

    Status Should Be    204                     ${resp}

    ${resp}             Get Unique Costumer     ${customer_id}

    Should Be Equal     ${resp.json()['address']}       Rua Miguel Ferreira, 100     


